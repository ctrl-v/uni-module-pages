import commonjs from "@rollup/plugin-commonjs";
import { babel } from "@rollup/plugin-babel";
import { nodeResolve } from "@rollup/plugin-node-resolve";
import typescript from "@rollup/plugin-typescript";
import terser from "@rollup/plugin-terser";
export default {
  input: "src/index.ts",
  output: [
    {
      file: "lib/index.cjs",
      format: "cjs",
      exports: "auto",
      plugins: [terser()],
    },
    {
      file: "lib/index.mjs",
      format: "esm",
      exports: "auto",
    },
  ],
  external: ["@dcloudio/uni-cli-shared"],
  plugins: [
    typescript({ tsconfig: "./tsconfig.json" }),
    commonjs(),
    babel({ babelHelpers: "bundled" }),
    nodeResolve(),
  ],
};
