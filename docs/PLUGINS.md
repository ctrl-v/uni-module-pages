# PLUGINS

插件|描述|仓库
--|--|---
[hook_push](https://www.npmjs.com/package/@ctrlc/hook_push)|语义化修改版本号，可选`tag`
[uni-module-pages](https://www.npmjs.com/package/@ctrlc/uni-module-pages)|`uniapp`项目处理`pages.json`模块化的路由插件|<a href='https://gitee.com/ctrl-v/uni-module-pages'><img src='https://gitee.com/ctrl-v/uni-module-pages/widgets/widget_6.svg?color=4183c4' alt='Fork me on Gitee'></img></a>
[vite-plugin-vue-setup-extend](https://www.npmjs.com/package/@ctrlc/vite-plugin-vue-script-attrs)| `vue3`在`<script setup>`语法时，扩展`name`以及`inheritAttrs`属性
[vite-plugin-vue-html-insert](https://www.npmjs.com/package/@ctrlc/vite-plugin-vue-html-insert)|用于`vue3`在`index.html`注入`css`、`js`
[storage](https://www.npmjs.com/package/@ctrlc/storage)| 网页缓存封装
[mouse-track](https://www.npmjs.com/package/@ctrlc/mouse-track)| [鼠标轨迹动画](https://yangzhuq.github.io/pages/)