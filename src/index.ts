import { existsSync, readdirSync, readFileSync } from "node:fs";
import { resolve } from "path";
import { parseJson } from "@dcloudio/uni-cli-shared";
import { normalizeUniModulesPagesJson } from "./utils";

export interface UniModulePagesOption {
  dir?: string;
  uniModules?: { id: string; path: string }[];
}

export default class UniModulePages {
  routerDir: string | undefined;
  routerFiles: { id: string; path: string }[] | undefined;
  constructor(option: UniModulePagesOption) {
    const uniDir = process.env.UNI_INPUT_DIR as string;
    let { dir, uniModules } = option;
    if (dir) {
      dir = resolve(uniDir, dir);
    }
    if (uniModules) {
      uniModules = uniModules.map((module) => {
        return {
          ...module,
          path: resolve(uniDir, `uni_modules/${module.id}/${module.path}`),
        };
      });
    }
    this.routerDir = dir;
    this.routerFiles = uniModules;
  }
  getPageJson(addDependency: (arg0: string) => void) {
    // router路由注入
    const routerDir = this.routerDir;
    const routerPagesJson = {
      pages: <any[]>[],
      subPackages: <any[]>[],
    };
    if (routerDir && existsSync(routerDir)) {
      let files = readdirSync(routerDir);
      files = files.filter((f) => {
        return f.endsWith(".json");
      });
      for (const file of files) {
        const routerPath = resolve(routerDir, file);
        const routerJson: typeof routerPagesJson = parseJson(
          readFileSync(routerPath).toString(),
          true
        );
        addDependency(routerPath);
        routerPagesJson.pages.push(...(routerJson.pages || []));
        routerPagesJson.subPackages.push(...(routerJson.subPackages || []));
      }
    }

    // uni_modules路由注入
    const routerFiles = this.routerFiles;
    const uniModulesPagesJson = {
      pages: <any[]>[],
      subPackages: <any[]>[],
    };

    routerFiles?.forEach((routerFile) => {
      if (existsSync(routerFile.path)) {
        const pagesJson: typeof uniModulesPagesJson =
          normalizeUniModulesPagesJson(
            parseJson(readFileSync(routerFile.path).toString(), true),
            routerFile.id
          );
        addDependency(routerFile.path);
        uniModulesPagesJson.pages.push(...(pagesJson.pages || []));
        uniModulesPagesJson.subPackages.push(...(pagesJson.subPackages || []));
      }
    });

    return {
      pages: [...routerPagesJson.pages, ...uniModulesPagesJson.pages],
      subPackages: [
        ...routerPagesJson.subPackages,
        ...uniModulesPagesJson.subPackages,
      ],
    };
  }
  loader(
    pagesJson: { pages: any[]; subPackages: any[] },
    { addDependency }: any
  ) {
    const { pages = [], subPackages = [] } = this.getPageJson(addDependency);
    pagesJson.pages = pagesJson.pages || [];
    pagesJson.subPackages = pagesJson.subPackages || [];
    pagesJson.pages.push(...pages);
    pagesJson.subPackages.push(...subPackages);
    return pagesJson;
  }
}
